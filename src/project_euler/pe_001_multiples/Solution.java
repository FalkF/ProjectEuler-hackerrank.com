package project_euler.pe_001_multiples;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Solution {

    private static final String input = "2 10 100";

    public static String getMultiples(ArrayList<Long> input) {
        ArrayList<Long> result = new ArrayList<>();

        input.forEach(n -> {
            n = n - 1;
            long a = n / 3;
            long b = n / 5;
            long doubles = n / 15;

            result.add(3 * a * (a + 1) / 2 + 5 * b * (b + 1) / 2 - 15 * doubles * (doubles + 1) / 2);
        });

        return result.stream().map(Object::toString).collect(Collectors.joining(System.lineSeparator()));
    }

    @SuppressWarnings("Duplicates")
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        if (!input.hasNextLong()) {
            throw new IllegalArgumentException("something went wrong");
        }
        Long testCaseCount = input.nextLong();

        ArrayList<Long> cases = new ArrayList<>();

        for (long i = 0; i < testCaseCount; i++) {
            if (!input.hasNextLong()) {
                throw new IllegalArgumentException("something went wrong");
            }
            cases.add(input.nextLong() - 1);
        }

        System.out.println(getMultiples(cases));
    }
}

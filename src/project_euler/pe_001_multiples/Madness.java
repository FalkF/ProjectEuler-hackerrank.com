package project_euler.pe_001_multiples;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Madness {

    public static String getMultiples(ArrayList<Long> cases) {
        ArrayList<Long> result = new ArrayList<>();

        cases.forEach(n -> {
            n = n - 1;
            // slow af (min time: 320ns vs 2560ns) and n-n%x == x*(n/x));
            //result.add(3 * (n / 3) * (n / 3 + 1) / 2 + 5 * (n / 5) * ((n / 5) + 1) / 2 - 15 * (n / 15) * ((n / 15) + 1) / 2);
            result.add((n - n % 3) * (n / 3 + 1) / 2 + (n - n % 5) * (n / 5 + 1) / 2 - (n - n % 15) * (n / 15 + 1) / 2);
        });

        return result.stream().map(Object::toString).collect(Collectors.joining(System.lineSeparator()));
    }

    @SuppressWarnings("Duplicates")
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        if (!input.hasNextLong()) {
            throw new IllegalArgumentException("something went wrong");
        }
        Long testCaseCount = input.nextLong();

        ArrayList<Long> cases = new ArrayList<>();

        for (long i = 0; i < testCaseCount; i++) {
            if (!input.hasNextLong()) {
                throw new IllegalArgumentException("something went wrong");
            }
            cases.add(input.nextLong() - 1);
        }

        System.out.println(getMultiples(cases));
    }
}

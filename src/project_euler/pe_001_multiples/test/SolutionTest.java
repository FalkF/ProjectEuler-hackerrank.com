package project_euler.pe_001_multiples.test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project_euler.pe_001_multiples.Madness;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class SolutionTest {

    private String result;

    @BeforeEach
    void prepare() {
        result = null;
    }

    @Test
    void testGetMultiples() {
        Long[] input = {10L, 100L};
        result = Madness.getMultiples(new ArrayList<>(Arrays.asList(input)));

        assertNotNull(result, "result should not be null");
        assertEquals("23" + System.lineSeparator() + "2318", result);
    }

}
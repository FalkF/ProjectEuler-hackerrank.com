import java.io.*;
import java.util.*;

public class Solution {
    
     private final static String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    
     public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
         
        if (!input.hasNextInt()) {
            throw new IllegalArgumentException("something went wrong");
        }
         
        Integer nameCount = input.nextInt();

        ArrayList<String> names = new ArrayList<>();

        for (int i = 0; i < nameCount; i++) {
            if (!input.hasNext()) {
                throw new IllegalArgumentException("something went wrong");
            }
            names.add(input.next());
        }
         
        if (!input.hasNextInt()) {
            throw new IllegalArgumentException("something went wrong");
        }
         
        Integer queryCount = input.nextInt();

        ArrayList<String> querys = new ArrayList<>();

        for (int i = 0; i < queryCount; i++) {
            if (!input.hasNext()) {
                throw new IllegalArgumentException("something went wrong");
            }
            querys.add(input.next());
        }
        
        Collections.sort(names); 
         
        querys.forEach(q -> System.out.println(nameScore(names, q)));
        
    }
    
    public static Integer nameScore(List names, String target) {
        char[] targetArray = target.toCharArray();
        Integer alphabeticalValue = 0;
        
        for (char c : targetArray) {
            alphabeticalValue += ALPHABET.indexOf(c) + 1;
        }
        
        return alphabeticalValue * (names.indexOf(target) + 1);
    }
    
}